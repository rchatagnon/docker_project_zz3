CREATE TABLE `results` (
  `id` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `result` float,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;