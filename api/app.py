import mysql.connector
from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
from flask import jsonify
import uuid
import json
import pika

app = Flask(__name__)


@app.route('/')
def index():
    return 'OK'

@app.route('/api/computations', methods=['GET', 'POST'])
def compute():
	if request.method == 'POST':
		# take data from form
		number1 = request.form['number1']
		number2 = request.form['number2']
		operator = request.form['operator']
		id = uuid.uuid4()
		# put it in json
		body = json.dumps({
			"number1": number1,
			"number2": number2,
			"operator": operator,
			"id": str(id)
			}
		)
		# send it to database
		mydb = mysql.connector.connect(
			host="db",
			user="root",
			password="root",
			database="computations"
		)
		cursor = mydb.cursor(prepared=True)
		sql_insert_query = """ INSERT INTO results (id, status, result) VALUES (%s,%s,%s)"""
		myTuple = (str(id), "Processing", None)

		cursor.execute(sql_insert_query, myTuple)
		mydb.commit()
		cursor.close()
		mydb.close()
		# send it to rabbitmq
		connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
		channel = connection.channel()
		channel.queue_declare(queue='task_queue', durable=True)
		channel.basic_publish(
			exchange='',
			routing_key='task_queue',
			body=body,
			properties=pika.BasicProperties(
				delivery_mode=2,  # make message persistent
			))
		connection.close()
		# redirect to result url
		return redirect(url_for('getRes', id = id))
	else:
		return render_template('computations.html')

@app.route('/api/computations/<uuid:id>')
def getRes(id):
	mydb = mysql.connector.connect(
		host="db",
		user="root",
		password="root",
		database="computations"
	)
	cursor = mydb.cursor(prepared=True)
	sql_select_query = """select * from results where id = %s"""
	cursor.execute(sql_select_query, (str(id),))
	record = cursor.fetchall()
	row_headers=[x[0] for x in cursor.description] #this will extract row headers
	json_data=[]
	for result in record:
		json_data.append(dict(zip(row_headers,result)))
	cursor.close()
	mydb.close()
	return jsonify(json_data)

@app.route('/api/results')
def get_results() :
	mydb = mysql.connector.connect(
		host="db",
		user="root",
		password="root",
		database="computations"
	)
	cursor = mydb.cursor()
	cursor.execute("SELECT * FROM results")
	row_headers=[x[0] for x in cursor.description] #this will extract row headers
	results = cursor.fetchall()
	json_data=[]
	for result in results:
		json_data.append(dict(zip(row_headers,result)))
	cursor.close()
	return jsonify(json_data)

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')