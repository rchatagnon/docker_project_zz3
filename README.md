# docker_project_zz3

## Description
Ce projet est proposé par M. Benjamin Rozière, dans le cadre de son cours de technologie des conteneurs à l'ISIMA pour les troisièmes années F5.

On souhaite réaliser une application de calcul via API où un utilisateur soumet son opération via un POST sur un endpoint de l’api. Le calcul, qui peut être long (dans le cadre de notre projet), est réalisé de manière asynchrone. L’API doit donc permettre à l’utilisateur de connaître l’état de son opération à tout moment. Enfin, l’utilisateur doit pouvoir obtenir le résultat de son opération, une fois celle-ci terminée.

## Spécifications
L’API est en HTTP REST, les messages sont au format json.

Les endpoints suivants sont implémentés :
- GET / (Renvoie OK -> test de fonctionnement)
- GET /api/computations (Formulaire pour demander un calcul)
- POST /api/computations (Demander un calcul)
- GET /api/computations/{id} (Récupérer le résultat et l'état d’un calcul)
- GET /api/results (Récupérer le résultat et l'état de tous les calculs)

Ce projet comporte 4 services :
- api, api en HTTP REST utilisant le framework Flask 
- computation, qui réalise les calculs, en Python
- rabbitmq, service de queue
- db, base de donnée MySQL

Un calcul est défini par 3 paramètres :
- id, son identifiant
- status, son état
- result, le résultat du calcul

L'état peut avoir 3 valeurs distinctes :
- Processing, le calcul est en cours
- Completed, le calcul est terminé, et le résultat est disponible
- Error, il y a une erreur, le calcul est annulé

## Utilisation
Démarrer les services :  
docker-compose -f "docker-compose.yml" up -d --build  

Proposer un calcul :  
Endpoint : http://localhost:5000/api/computations  
En utilisant un navigateur, l'utilisateur a accès à un formulaire. En cliquant sur "submit", l'utilisateur est automatiquement redirigé vers l'état et le résultat du calcul.  
L'utilisateur peut également utiliser la commande curl, par exemple : 
```curl -X POST -d 'number1=1' --data-urlencode 'operator=+' -d 'number2=5' http://localhost:5000/api/computations```


L'utilisateur peut également accéder à l'état et au résultat du calcul en mettant l'id du calcul dans l'url :  
http://localhost:5000/api/computations/f930932a-1658-4781-a3de-a355beb3dea9

Et il peut également voir tous les résultats : 
http://localhost:5000/api/results
