import mysql.connector
import pika
import time
import json

sleepTime = 20
print(' [*] Sleeping for ', sleepTime, ' seconds.')
time.sleep(sleepTime)

print(' [*] Connecting to server ...')
connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq'))
channel = connection.channel()
channel.queue_declare(queue='task_queue', durable=True)

print(' [*] Waiting for messages.')


def callback(ch, method, properties, body):
    # receive data
    print(" [x] Received %s" % body)
    cmd = json.loads(body.decode())
    operator = cmd["operator"]
    id = cmd["id"]
    try:
        number1 = float(cmd["number1"])
        number2 = float(cmd["number2"])
        # process data
        if operator == '+':
            res = number1 + number2
        elif operator == '-':
            res = number1 - number2
        elif operator == 'x':
            res = number1 * number2
        elif operator == '/':
            res = number1 / number2
        else:
            raise ValueError
    except (ValueError, ZeroDivisionError):
        res = None
        pass
    # send ack
    ch.basic_ack(delivery_tag=method.delivery_tag)
    # send it to database
    mydb = mysql.connector.connect(
        host="db",
        user="root",
        password="root",
        database="computations"
    )
    cursor = mydb.cursor(prepared=True)
    if res:
        sql_update_query = """UPDATE results set status= %s, result = %s where id = %s"""
        myTuple = ("Completed", res, str(id))
    else:
        sql_update_query = """UPDATE results set status= %s where id = %s"""
        myTuple = ("Error", str(id))
    cursor.execute(sql_update_query, myTuple)
    mydb.commit()
    cursor.close()
    mydb.close()

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='task_queue', on_message_callback=callback)
channel.start_consuming()